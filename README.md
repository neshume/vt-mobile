
#Virtual Textures For Mobile Devices

----

##About:

**vt-mobile** is an attempt to bring [Virtual Texturing][1] to mobile platforms based on OpenGL-ES,
such as iOS. Currently, the project is still in an early stage of development, however, most of the
basic Virtual Texturing components are already implemented and functioning.

Following are a few screenshots from the demo apps:

**vtDemoSimple:**

![vtDemoSimple](https://bytebucket.org/glampert/vt-mobile/raw/98c01113a44764b334aee89b64b001b6f274a175/docs/screens/demo_simple.png "vtDemoSimple")

**vtDemoPlanets:** (~3GB of texture data)

![vtDemoPlanets](https://bytebucket.org/glampert/vt-mobile/raw/98c01113a44764b334aee89b64b001b6f274a175/docs/screens/demo_planets1.png "vtDemoPlanets")

**vtDemoPlanets:** (debug page visualization)

![vtDemoPlanets debug view](https://bytebucket.org/glampert/vt-mobile/raw/98c01113a44764b334aee89b64b001b6f274a175/docs/screens/demo_planets3.png "vtDemoPlanets debug view")

**NOTE:** Demos in the images above are running on the iOS simulator inside XCode.
Tests running on the actual devices present real-time frame rates (30+ fps).

##Source Code:

The project is written in **C++ 11** using modern programming practices and attempting
to be as "memory safe" as possible, without sacrificing performance.

The code is mostly portable, but currently has a few iOS specific dependencies (namely [GCD][3] and some GL-ES Apple extensions).
Currently, the only build system provided are XCode projects used by the demos applications.

##Directory Structure:

    +-vt-mobile/
     |
     +-docs/       => Documentation and papers written in Brazilian Portuguese for my university course.
     |
     +-source/     => Main source code path for the whole project.
      |
      +-demos/     => Demo applications for the iOS platform. Includes XCode projects.
      |
      +-vt_lib/    => Main reusable components of the Virtual Texturing system.
       |
       +-glsl/     => OpenGL-ES GLSL shaders.
       |
       +-include/  => C++ header files for vt_lib.
       |
      ++-source/   => C++ source files for vt_lib.
      |
      +-vt_tools/  => Off-line texture processing tools and pipeline.
       |
       +-include/  => C++ include files for vt_tools.
       |
       +-source/   => C++ source files for vt_tools and the vtmake command-line utility.

##License:

This project's source code is released under the [MIT License][2].


[1]: http://en.wikipedia.org/wiki/MegaTexture
[2]: http://opensource.org/licenses/MIT
[3]: http://en.wikipedia.org/wiki/Grand_Central_Dispatch

